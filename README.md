# Curso Intermediário de Angular - Digital Innovation One

Esse curso foi feito para a plataforma [Digital Innovation One](https://digitalinnovation.one/)

O curso consiste em um sistema de filmes, com a possibilidade de cadastros, edições, listagem e visualização dos filmes.

## Instalação

1. clone o repositório `git clone git@github.com:RenanRB/curso-angular.git`
2. Entre no projeto e instale as dependencias `npm install`

## Ambiente Local

Execute `ng serve` para que o projeto suba localmente. Acesse a url `http://localhost:4200/`. O projeto já está com reload automático conforme as alterações que você realizar no código

## Simulando o Back-end

Execute `npm install -g json-server` para instalar globalmente o servidor json. Após a instalação entre na pasta do projeto e execute `json-server --watch db.json`, com isso um servidor será inicializado na url `http://localhost:3000/`, após a inicialização sera possível realizar requisições http.

## Requisições Http

- GET `http://localhost:3000/filmes` para listar filmes
- POST `http://localhost:3000/filmes` para salvar um filme
- GET `http://localhost:3000/filmes/1` para consultar um filme
- PUT `http://localhost:3000/filmes/1` para atualizar um filme
- DELETE `http://localhost:3000/filmes/1` para remover um filme


Para listar filmes acesse a URL `http://localhost:3000/filmes` e obtenha a seguinte resposta:

```json
[
  {
    "id": 1,
    "titulo": "MIB: Homens de Preto",
    "urlFoto": "https://m.media-amazon.com/images/M/MV5BOTlhYTVkMDktYzIyNC00NzlkLTlmN2ItOGEyMWQ4OTA2NDdmXkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_UX182_CR0,0,182,268_AL_.jpg",
    "dtLancamento": "1997-07-11T03:00:00.000Z",
    "descricao": "A police officer joins a secret organization that polices and monitors extraterrestrial interactions on Earth.",
    "nota": 7.3,
    "urlIMDb": "https://www.imdb.com/title/tt0119654/",
    "genero": "Ação"
  },
  {
    "id": 2,
    "titulo": "MIB: Homens de Preto II ",
    "urlFoto": "https://m.media-amazon.com/images/M/MV5BMTMxNDA0NTMxMV5BMl5BanBnXkFtZTYwMDE2NzY2._V1_UX182_CR0,0,182,268_AL_.jpg",
    "dtLancamento": "2012-07-19T03:00:00.000Z",
    "descricao": "Agent J is sent to find Agent K and restore his memory after the re-appearance of a case from K's past.",
    "nota": 6.2,
    "urlIMDb": "https://www.imdb.com/title/tt0120912",
    "genero": "Ação"
  },
  {
    "id": 3,
    "titulo": "MIB: Homens de Preto III",
    "urlFoto": "https://m.media-amazon.com/images/M/MV5BMTU2NTYxODcwMF5BMl5BanBnXkFtZTcwNDk1NDY0Nw@@._V1_UX182_CR0,0,182,268_AL_.jpg",
    "dtLancamento": "2012-25-11T03:00:00.000Z",
    "descricao": "Agent J travels in time to M.I.B.'s early days in 1969 to stop an alien from assassinating his friend Agent K and changing history.",
    "nota": 6.8,
    "urlIMDb": "https://www.imdb.com/title/tt1409024/",
    "genero": "Ação"
  }
]
```

Para consultar um registro acesse a URL `http://localhost:3000/filmes/1` e obtenha a seguinte resposta:

```json
{
    "id": 1,
    "titulo": "MIB: Homens de Preto",
    "urlFoto": "https://m.media-amazon.com/images/M/MV5BOTlhYTVkMDktYzIyNC00NzlkLTlmN2ItOGEyMWQ4OTA2NDdmXkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_UX182_CR0,0,182,268_AL_.jpg",
    "dtLancamento": "1997-07-11T03:00:00.000Z",
    "descricao": "A police officer joins a secret organization that polices and monitors extraterrestrial interactions on Earth.",
    "nota": 7.3,
    "urlIMDb": "https://www.imdb.com/title/tt0119654/",
    "genero": "Ação"
}
```

Para facilitar a consulta de dados na API podemos usar as seguintes ferramentas:

- Postman
- Curl
- Swagger

## Gerando componente

Execute `ng generate component nome-do-componente` para criar um novo componente. Você também pode usuar `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Execute `ng build` para gerar o compilado do projeto. O projeto vai ser criado dentro do diretório `dist/`. Adicionar `--prod` junto comando de build para gerar minificado e pronto para o ambiente de produção.

